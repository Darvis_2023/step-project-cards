export class Visit {
  constructor(id, patientName, doctor, purpose, description, urgency) {
    this.data = {
      id,
      patientName,
      doctor,
      purpose,
      description,
      urgency,
    };
  }

  render() {
    this.wrapper = document.createElement("div");
    this.wrapper.classList.add("card-wrapper");

    this.showFirstInfo = document.createElement("div");

    this.cardHeader = document.createElement("h3");
    this.cardHeader.classList.add("card-header");
    this.cardHeader.innerText = `Ім'я пацієнта: ${this.data.patientName}`;

    this.doctor = document.createElement("p");
    this.doctor.innerText = `Доктор: ${this.data.doctor}`;

    this.showMoreBtn = document.createElement("button");
    this.showMoreBtn.classList.add("btn", "btn-primary", "show-more-btn");
    this.showMoreBtn.innerText = "Більше";

    this.hideBtn = document.createElement("button");
    this.hideBtn.classList.add("hidden", "hide-btn", "btn", "btn-primary");
    this.hideBtn.innerText = "Сховати";

    this.showFirstInfo.append(
      this.cardHeader,
      this.doctor,
      this.showMoreBtn,
      this.hideBtn
    );

    this.showMoreInfo = document.createElement("div");
    this.showMoreInfo.classList.add("show-more-info");

    this.showMoreBtn.addEventListener("click", (e) => {
      this.showMoreInfo.classList.toggle("active");
      this.showMoreBtn.classList.add("hidden");
      this.hideBtn.classList.remove("hidden");
    });

    this.hideBtn.addEventListener("click", (e) => {
      this.showMoreInfo.classList.toggle("active");
      this.showMoreBtn.classList.remove("hidden");
      this.hideBtn.classList.add("hidden");
    });

    this.purpose = document.createElement("p");
    this.purpose.innerText = `Ціль візиту: ${this.data.purpose}`;

    this.description = document.createElement("p");
    this.description.classList.add("card__description");
    this.description.innerText = `Опис візиту: ${this.data.description}`;

    this.urgency = document.createElement("p");
    this.urgency.classList.add("card__priority");
    this.urgency.innerText = `Терміновість: ${this.data.urgency}`;
    console.log(this.data.urgency);

    this.showMoreInfo.append(this.purpose, this.description, this.urgency);

    this.btnContainer = document.createElement("div");
    this.btnContainer.classList.add("btn-container");

    this.editBtn = document.createElement("div");
    this.editBtn.classList.add("edit-btn", "btn", "btn-outline-success");
    this.editBtn.innerText = "Редагувати";

    this.deleteBtn = document.createElement("div");
    this.deleteBtn.classList.add("delete-btn", "btn", "btn-outline-danger");
    this.deleteBtn.innerText = "Видалити";

    let hiddenId = document.createElement("p");
    hiddenId.setAttribute("hidden", "");
    hiddenId.classList.add("hidden-id");
    hiddenId.innerText = this.data.id;

    this.btnContainer.append(this.editBtn, this.deleteBtn, hiddenId);
    this.wrapper.append(
      this.showFirstInfo,
      this.showMoreInfo,
      this.btnContainer
    );

    return this.wrapper;
  }
}
