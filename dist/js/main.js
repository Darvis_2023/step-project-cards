/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./src/js/Modal-login.js":
/*!*******************************!*\
  !*** ./src/js/Modal-login.js ***!
  \*******************************/
/***/ (() => {

const openPopUp = document.querySelector(".open_pop_up");
const closePopUp = document.querySelector(".pop_up_close");
const popUp = document.querySelector(".pop_up_authorization");
openPopUp.addEventListener("click", e => {
  e.preventDefault();
  popUp.style.display = "block";
});
closePopUp.addEventListener("click", () => {
  popUp.style.display = "none";
});
function showPass() {
  const show = document.querySelector(".pass-btn");
  const input = document.querySelector(".password-input");
  show.addEventListener("click", () => {
    show.classList.toggle('active');
    if (input.getAttribute('type') === 'password') {
      input.setAttribute('type', 'text');
    } else {
      input.setAttribute('type', 'password');
    }
  });
}
showPass();

/***/ }),

/***/ "./src/js/Visits/Visit.js":
/*!********************************!*\
  !*** ./src/js/Visits/Visit.js ***!
  \********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Visit": () => (/* binding */ Visit)
/* harmony export */ });
class Visit {
  constructor(id, patientName, doctor, purpose, description, urgency) {
    this.data = {
      id,
      patientName,
      doctor,
      purpose,
      description,
      urgency
    };
  }
  render() {
    this.wrapper = document.createElement("div");
    this.wrapper.classList.add("card-wrapper");
    this.showFirstInfo = document.createElement("div");
    this.cardHeader = document.createElement("h3");
    this.cardHeader.classList.add("card-header");
    this.cardHeader.innerText = `Ім'я пацієнта: ${this.data.patientName}`;
    this.doctor = document.createElement("p");
    this.doctor.innerText = `Доктор: ${this.data.doctor}`;
    this.showMoreBtn = document.createElement("button");
    this.showMoreBtn.classList.add("btn", "btn-primary", "show-more-btn");
    this.showMoreBtn.innerText = "Більше";
    this.hideBtn = document.createElement("button");
    this.hideBtn.classList.add("hidden", "hide-btn", "btn", "btn-primary");
    this.hideBtn.innerText = "Сховати";
    this.showFirstInfo.append(this.cardHeader, this.doctor, this.showMoreBtn, this.hideBtn);
    this.showMoreInfo = document.createElement("div");
    this.showMoreInfo.classList.add("show-more-info");
    this.showMoreBtn.addEventListener("click", e => {
      this.showMoreInfo.classList.toggle("active");
      this.showMoreBtn.classList.add("hidden");
      this.hideBtn.classList.remove("hidden");
    });
    this.hideBtn.addEventListener("click", e => {
      this.showMoreInfo.classList.toggle("active");
      this.showMoreBtn.classList.remove("hidden");
      this.hideBtn.classList.add("hidden");
    });
    this.purpose = document.createElement("p");
    this.purpose.innerText = `Ціль візиту: ${this.data.purpose}`;
    this.description = document.createElement("p");
    this.description.classList.add("card__description");
    this.description.innerText = `Опис візиту: ${this.data.description}`;
    this.urgency = document.createElement("p");
    this.urgency.classList.add("card__priority");
    this.urgency.innerText = `Терміновість: ${this.data.urgency}`;
    console.log(this.data.urgency);
    this.showMoreInfo.append(this.purpose, this.description, this.urgency);
    this.btnContainer = document.createElement("div");
    this.btnContainer.classList.add("btn-container");
    this.editBtn = document.createElement("div");
    this.editBtn.classList.add("edit-btn", "btn", "btn-outline-success");
    this.editBtn.innerText = "Редагувати";
    this.deleteBtn = document.createElement("div");
    this.deleteBtn.classList.add("delete-btn", "btn", "btn-outline-danger");
    this.deleteBtn.innerText = "Видалити";
    let hiddenId = document.createElement("p");
    hiddenId.setAttribute("hidden", "");
    hiddenId.classList.add("hidden-id");
    hiddenId.innerText = this.data.id;
    this.btnContainer.append(this.editBtn, this.deleteBtn, hiddenId);
    this.wrapper.append(this.showFirstInfo, this.showMoreInfo, this.btnContainer);
    return this.wrapper;
  }
}

/***/ }),

/***/ "./src/js/Visits/VisitCardiologist.js":
/*!********************************************!*\
  !*** ./src/js/Visits/VisitCardiologist.js ***!
  \********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "VisitCardiologist": () => (/* binding */ VisitCardiologist)
/* harmony export */ });
/* harmony import */ var _Visit__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Visit */ "./src/js/Visits/Visit.js");

class VisitCardiologist extends _Visit__WEBPACK_IMPORTED_MODULE_0__.Visit {
  constructor(id, patientName, doctor, purpose, description, urgency, pressure, bodyMassIndex, pastDiseases, age) {
    super(id, patientName, doctor, purpose, description, urgency);
    this.data = {
      ...this.data,
      pressure,
      bodyMassIndex,
      pastDiseases,
      age
    };
  }
  render() {
    super.render();
    this.pressure = document.createElement('p');
    this.pressure.innerText = `Артеріальний тиск: ${this.data.pressure}`;
    this.bodyMassIndex = document.createElement('p');
    this.bodyMassIndex.innerText = `Індекс маси тіла: ${this.data.bodyMassIndex}`;
    this.pastDiseases = document.createElement('p');
    this.pastDiseases.innerText = `Хвороби: ${this.data.pastDiseases}`;
    this.age = document.createElement('p');
    this.age.innerText = `Вік: ${this.data.age}`;
    this.showMoreInfo.append(this.pressure, this.bodyMassIndex, this.pastDiseases, this.age);
    return this.wrapper;
  }
}

/***/ }),

/***/ "./src/js/Visits/VisitDentist.js":
/*!***************************************!*\
  !*** ./src/js/Visits/VisitDentist.js ***!
  \***************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "VisitDentist": () => (/* binding */ VisitDentist)
/* harmony export */ });
/* harmony import */ var _Visit__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Visit */ "./src/js/Visits/Visit.js");

class VisitDentist extends _Visit__WEBPACK_IMPORTED_MODULE_0__.Visit {
  constructor(id, patientName, doctor, purpose, description, urgency, lastVisit) {
    super(id, patientName, doctor, purpose, description, urgency);
    this.data = {
      ...this.data,
      lastVisit
    };
  }
  render() {
    super.render();
    this.lastVisit = document.createElement('p');
    this.lastVisit.innerText = `Останній візит: ${this.data.lastVisit}`;
    this.showMoreInfo.append(this.lastVisit);
    return this.wrapper;
  }
}

/***/ }),

/***/ "./src/js/Visits/VisitTherapist.js":
/*!*****************************************!*\
  !*** ./src/js/Visits/VisitTherapist.js ***!
  \*****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "VisitTherapist": () => (/* binding */ VisitTherapist)
/* harmony export */ });
/* harmony import */ var _Visit__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Visit */ "./src/js/Visits/Visit.js");

class VisitTherapist extends _Visit__WEBPACK_IMPORTED_MODULE_0__.Visit {
  constructor(id, patientName, doctor, purpose, description, urgency, age) {
    super(id, patientName, doctor, purpose, description, urgency);
    this.data = {
      ...this.data,
      age
    };
  }
  render() {
    super.render();
    this.age = document.createElement("p");
    this.age.innerText = `Вік: ${this.data.age}`;
    this.showMoreInfo.append(this.age);
    return this.wrapper;
  }
}

/***/ }),

/***/ "./src/js/components/Input.js":
/*!************************************!*\
  !*** ./src/js/components/Input.js ***!
  \************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ Input)
/* harmony export */ });
function _defineProperty(obj, key, value) { key = _toPropertyKey(key); if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
function _toPropertyKey(arg) { var key = _toPrimitive(arg, "string"); return typeof key === "symbol" ? key : String(key); }
function _toPrimitive(input, hint) { if (typeof input !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (typeof res !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }
class Input {
  constructor(labelText, type, placeholder, isRequired, className, name) {
    let additionalAttributes = arguments.length > 6 && arguments[6] !== undefined ? arguments[6] : [];
    _defineProperty(this, "input", void 0);
    this.type = type;
    this.placeholder = placeholder;
    this.isRequired = isRequired;
    this.className = className;
    this.name = name;
    this.additionalAttributes = additionalAttributes;
    this.self = document.createElement('div');
    this.labelText = labelText;
    this.label = document.createElement('label');
    this.input = document.createElement('input');
  }
  set value(text) {
    this.input.value = text;
  }
  render() {
    if (this.type === "hidden") this.self.classList.add("d-none");
    this.label.classList.add('form-label');
    this.label.innerText = this.labelText;
    this.self.append(this.label);
    this.input.setAttribute('type', this.type);
    this.input.setAttribute('placeholder', this.placeholder);
    this.input.setAttribute('name', this.name);
    this.input.required = this.isRequired;
    this.input.classList.add(this.className);
    this.additionalAttributes.forEach(attr => {
      this.input.setAttribute(attr.name, attr.value);
    });
    this.self.append(this.input);
    return this.self;
  }
}

/***/ }),

/***/ "./src/js/components/Select.js":
/*!*************************************!*\
  !*** ./src/js/components/Select.js ***!
  \*************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ Select)
/* harmony export */ });
class Select {
  constructor(options, labelText, isRequired, className, name) {
    this.isRequired = isRequired;
    this.self = document.createElement("div");
    this.label = document.createElement('label');
    this.select = document.createElement("select");
    const defaultOpt = document.createElement("option");
    defaultOpt.textContent = "Зробіть вибір...";
    defaultOpt.disabled = true;
    defaultOpt.selected = true;
    this.select.append(defaultOpt);
    options.forEach(opt => {
      const optionNode = document.createElement("option");
      optionNode.textContent = opt;
      this.select.append(optionNode);
    });
    this.name = name;
    this.labelText = labelText;
    this.className = className;
  }
  set value(itemName) {
    for (let option of this.select.options) {
      if (option.text === itemName) {
        option.selected = true;
      }
    }
  }
  render() {
    this.label.classList.add('form-label');
    this.label.innerText = this.labelText;
    this.self.append(this.label);
    this.select.classList.add(this.className);
    this.select.setAttribute('name', this.name);
    this.select.required = this.isRequired;
    this.self.append(this.select);
    return this.self;
  }
}
;

/***/ }),

/***/ "./src/js/components/Textarea.js":
/*!***************************************!*\
  !*** ./src/js/components/Textarea.js ***!
  \***************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ Textarea)
/* harmony export */ });
class Textarea {
  constructor(labelText, rows, placeholder, isRequired, className, name) {
    let additionalAttributes = arguments.length > 6 && arguments[6] !== undefined ? arguments[6] : [];
    this.rows = rows;
    this.placeholder = placeholder;
    this.isRequired = isRequired;
    this.className = className;
    this.name = name;
    this.additionalAttributes = additionalAttributes;
    this.self = document.createElement('div');
    this.labelText = labelText;
    this.label = document.createElement('label');
    this.textarea = document.createElement('textarea');
  }
  set value(text) {
    this.textarea.value = text;
  }
  render() {
    this.label.classList.add('form-label');
    this.label.innerText = this.labelText;
    this.self.append(this.label);
    this.textarea.setAttribute('rows', this.rows);
    this.textarea.setAttribute('placeholder', this.placeholder);
    this.textarea.setAttribute('name', this.name);
    this.textarea.required = this.isRequired;
    this.textarea.classList.add(this.className);
    this.additionalAttributes.forEach(attr => {
      this.textarea.setAttribute(attr.name, attr.value);
    });
    this.self.append(this.textarea);
    return this.self;
  }
}

/***/ }),

/***/ "./src/js/filters/inputFilter.js":
/*!***************************************!*\
  !*** ./src/js/filters/inputFilter.js ***!
  \***************************************/
/***/ (() => {

const wrapperFilter = document.querySelector(".filter__form");
const changeStr = el => el.textContent.split(":")[1];
wrapperFilter.addEventListener("input", event => {
  const colectionCards = document.querySelectorAll(".card-wrapper");
  const cards = [...colectionCards];
  const inputDesc = document.querySelector(".filter__search");
  const inputSelect = document.querySelector(".filter__priority");
  cards.filter(card => {
    let cardDesc = card.querySelector(".card__description");
    let cardPrior = card.querySelector(".card__priority");
    let textDesc = changeStr(cardDesc);
    let textPrior = changeStr(cardPrior);
    if (textDesc.includes(inputDesc.value)) {
      card.style.opacity = "1";
    } else {
      card.style.opacity = "0";
    }
    if (textPrior.trim() === inputSelect.value || inputSelect.value === "Оберіть пріоритетність") {
      card.style.display = "block";
    } else {
      card.style.display = "none";
    }
  });
});

/***/ }),

/***/ "./src/js/forms/Form.js":
/*!******************************!*\
  !*** ./src/js/forms/Form.js ***!
  \******************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Form": () => (/* binding */ Form)
/* harmony export */ });
/* harmony import */ var _components_Select__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../components/Select */ "./src/js/components/Select.js");
/* harmony import */ var _utils_info__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../utils/info */ "./src/js/utils/info.js");
/* harmony import */ var _components_Input__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../components/Input */ "./src/js/components/Input.js");
/* harmony import */ var _components_Textarea__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../components/Textarea */ "./src/js/components/Textarea.js");




class Form {
  constructor(doctor) {
    this.self = document.createElement('form');
    let doctorInput = new _components_Input__WEBPACK_IMPORTED_MODULE_2__["default"]("", "text", "", false, "form-control", "doctor", [{
      name: "readonly",
      value: ""
    }, {
      name: "hidden",
      value: ""
    }]);
    doctorInput.value = doctor;
    this.doctor = doctorInput;
    this.purpose = new _components_Input__WEBPACK_IMPORTED_MODULE_2__["default"]('Ціль візиту:', 'text', 'Ціль візиту', true, 'form-control', 'purpose');
    this.description = new _components_Textarea__WEBPACK_IMPORTED_MODULE_3__["default"]('Опис візиту:', '3', 'Опис візиту', true, 'form-control', 'description');
    this.urgency = new _components_Select__WEBPACK_IMPORTED_MODULE_0__["default"](_utils_info__WEBPACK_IMPORTED_MODULE_1__.info.urgency, 'Терміновість', true, 'form-select', 'urgency');
    this.patientName = new _components_Input__WEBPACK_IMPORTED_MODULE_2__["default"]('ПІБ:', 'text', 'ПІБ', true, 'form-control', 'patientName');
  }
  render() {
    this.self.append(this.doctor.render(), this.purpose.render(), this.description.render(), this.urgency.render(), this.patientName.render());
    return this.self;
  }
  renderForEdit(id, purpose, description, urgency, patientName) {
    let hiddenId = new _components_Input__WEBPACK_IMPORTED_MODULE_2__["default"]("", "hidden", "", false, "hidden-edit-id", "id");
    hiddenId.value = id;
    this.purpose.value = purpose;
    this.description.value = description;
    this.urgency.value = urgency;
    this.patientName.value = patientName;
    this.self.append(hiddenId.render(), this.doctor.render(), this.purpose.render(), this.description.render(), this.urgency.render(), this.patientName.render());
    return this.self;
  }
}

/***/ }),

/***/ "./src/js/forms/FormCardiologist.js":
/*!******************************************!*\
  !*** ./src/js/forms/FormCardiologist.js ***!
  \******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "FormCardiologist": () => (/* binding */ FormCardiologist)
/* harmony export */ });
/* harmony import */ var _components_Input__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../components/Input */ "./src/js/components/Input.js");
/* harmony import */ var _components_Textarea__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../components/Textarea */ "./src/js/components/Textarea.js");
/* harmony import */ var _Form__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Form */ "./src/js/forms/Form.js");
/* harmony import */ var _utils_info__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../utils/info */ "./src/js/utils/info.js");




class FormCardiologist extends _Form__WEBPACK_IMPORTED_MODULE_2__.Form {
  constructor() {
    super(_utils_info__WEBPACK_IMPORTED_MODULE_3__.info.cardiologist);
    this.pressure = new _components_Input__WEBPACK_IMPORTED_MODULE_0__["default"]('Тиск:', 'text', 'Тиск', false, 'form-control', 'pressure');
    this.bodyMassIndex = new _components_Input__WEBPACK_IMPORTED_MODULE_0__["default"]('Індекс маси тіла:', 'text', 'Індекс маси тіла', false, 'form-control', 'bodyMassIndex');
    this.pastDiseases = new _components_Textarea__WEBPACK_IMPORTED_MODULE_1__["default"]('Перенесені захворювання:', '3', 'Перенесені захворювання', false, 'form-control', 'pastDiseases');
    this.age = new _components_Input__WEBPACK_IMPORTED_MODULE_0__["default"]('Вік:', 'text', 'Вік', false, 'form-control', 'age');
  }
  render() {
    super.render();
    this.self.append(this.pressure.render(), this.bodyMassIndex.render(), this.pastDiseases.render(), this.age.render());
    return this.self;
  }
  renderForEdit(id, purpose, description, urgency, patientName, pressure, bodyMassIndex, pastDiseases, age) {
    super.renderForEdit(id, purpose, description, urgency, patientName);
    this.pressure.value = pressure;
    this.bodyMassIndex.value = bodyMassIndex;
    this.pastDiseases.value = pastDiseases;
    this.age.value = age;
    this.self.append(this.pressure.render(), this.bodyMassIndex.render(), this.pastDiseases.render(), this.age.render());
    return this.self;
  }
}

/***/ }),

/***/ "./src/js/forms/FormDentist.js":
/*!*************************************!*\
  !*** ./src/js/forms/FormDentist.js ***!
  \*************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "FormDentist": () => (/* binding */ FormDentist)
/* harmony export */ });
/* harmony import */ var _components_Input__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../components/Input */ "./src/js/components/Input.js");
/* harmony import */ var _utils_info__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../utils/info */ "./src/js/utils/info.js");
/* harmony import */ var _Form__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Form */ "./src/js/forms/Form.js");



class FormDentist extends _Form__WEBPACK_IMPORTED_MODULE_2__.Form {
  constructor() {
    super(_utils_info__WEBPACK_IMPORTED_MODULE_1__.info.dentist);
    this.lastVisit = new _components_Input__WEBPACK_IMPORTED_MODULE_0__["default"]('Дата останнього візиту:', 'date', ' ', false, 'last-visit', 'lastVisit');
  }
  render() {
    super.render();
    this.self.append(this.lastVisit.render());
    return this.self;
  }
  renderForEdit(id, purpose, description, urgency, patientName, lastVisit) {
    super.renderForEdit(id, purpose, description, urgency, patientName);
    this.lastVisit.value = lastVisit;
    this.self.append(this.lastVisit.render());
    return this.self;
  }
}

/***/ }),

/***/ "./src/js/forms/FormTherapist.js":
/*!***************************************!*\
  !*** ./src/js/forms/FormTherapist.js ***!
  \***************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "FormTherapist": () => (/* binding */ FormTherapist)
/* harmony export */ });
/* harmony import */ var _components_Input__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../components/Input */ "./src/js/components/Input.js");
/* harmony import */ var _Form__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Form */ "./src/js/forms/Form.js");
/* harmony import */ var _utils_info__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../utils/info */ "./src/js/utils/info.js");



class FormTherapist extends _Form__WEBPACK_IMPORTED_MODULE_1__.Form {
  constructor() {
    super(_utils_info__WEBPACK_IMPORTED_MODULE_2__.info.therapist);
    this.age = new _components_Input__WEBPACK_IMPORTED_MODULE_0__["default"]('Вік:', 'text', 'Вік', false, 'form-control', 'age');
  }
  render() {
    super.render();
    this.self.append(this.age.render());
    return this.self;
  }
  renderForEdit(id, purpose, description, urgency, patientName, age) {
    super.renderForEdit(id, purpose, description, urgency, patientName);
    this.age.value = age;
    this.self.append(this.age.render());
    return this.self;
  }
}

/***/ }),

/***/ "./src/js/loginInput.js":
/*!******************************!*\
  !*** ./src/js/loginInput.js ***!
  \******************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _utils_API__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./utils/API */ "./src/js/utils/API.js");
/* harmony import */ var _utils_token__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./utils/token */ "./src/js/utils/token.js");


const loginInput = document.querySelector(".login-input");
const passInput = document.querySelector(".password-input");
const submitBtn = document.querySelector(".submit");
submitBtn.addEventListener("click", () => {
  (0,_utils_API__WEBPACK_IMPORTED_MODULE_0__.login)(loginInput.value, passInput.value).then(response => response.text()).then(token => {
    if (token.includes("-")) {
      (0,_utils_token__WEBPACK_IMPORTED_MODULE_1__.setToken)(token);
      document.location.href = "./index.html";
    } else {
      alert("Password or login is incorrect");
    }
  }).catch(() => {
    alert("Password or login is incorrect");
  });
});

/***/ }),

/***/ "./src/js/main.js":
/*!************************!*\
  !*** ./src/js/main.js ***!
  \************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "modal": () => (/* binding */ modal)
/* harmony export */ });
/* harmony import */ var _Modal_login__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Modal-login */ "./src/js/Modal-login.js");
/* harmony import */ var _Modal_login__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_Modal_login__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _loginInput__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./loginInput */ "./src/js/loginInput.js");
/* harmony import */ var _modals_ModalVisit__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./modals/ModalVisit */ "./src/js/modals/ModalVisit.js");
/* harmony import */ var _utils_token__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./utils/token */ "./src/js/utils/token.js");
/* harmony import */ var _filters_inputFilter__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./filters/inputFilter */ "./src/js/filters/inputFilter.js");
/* harmony import */ var _filters_inputFilter__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_filters_inputFilter__WEBPACK_IMPORTED_MODULE_4__);






const logOutBtn = document.querySelector(".btn-exit");
const btnCreateVisit = document.querySelector(".btn-create");
var modal;
document.addEventListener("DOMContentLoaded", () => {
  if ((0,_utils_token__WEBPACK_IMPORTED_MODULE_3__.getToken)()) {
    modal = new _modals_ModalVisit__WEBPACK_IMPORTED_MODULE_2__.ModalVisit(document.body);
    (0,_modals_ModalVisit__WEBPACK_IMPORTED_MODULE_2__["default"])();
    let bntAuthorised = document.querySelector(".btns-authorised");
    bntAuthorised.classList.add("show");
    let btnNonAuthorised = document.querySelector(".btn-non-authorised");
    btnNonAuthorised.classList.remove("show");
    let filter = document.querySelector(".filter__block");
    filter.style.display = "flex";
  } else {
    let bntAuthorised = document.querySelector(".btns-authorised");
    bntAuthorised.classList.remove("show");
    let btnNonAuthorised = document.querySelector(".btn-non-authorised");
    btnNonAuthorised.classList.add("show");
    let hiddenFilter = document.querySelector(".filter__block");
    hiddenFilter.style.display = "none";
  }
});
btnCreateVisit.addEventListener("click", e => {
  e.preventDefault();
  modal.show();
});
logOutBtn.addEventListener("click", e => {
  e.preventDefault();
  (0,_utils_token__WEBPACK_IMPORTED_MODULE_3__.dropToken)();
  document.location.href = "./index.html";
});

/***/ }),

/***/ "./src/js/modals/Modal.js":
/*!********************************!*\
  !*** ./src/js/modals/Modal.js ***!
  \********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Modal": () => (/* binding */ Modal)
/* harmony export */ });
class Modal {
  constructor(parent) {
    this.wrapper = document.createElement('div');
    this.wrapper.setAttribute('id', 'myModal');
    this.wrapper.classList.add('modal', 'fade');
    this.wrapper.setAttribute('tabindex', '-1');
    this.wrapper.setAttribute('role', 'dialog');
    parent.append(this.wrapper);
    this.dialog = document.createElement('div');
    this.dialog.classList.add('modal-dialog', 'modal-dialog-centered', 'modal-lg');
    this.dialog.setAttribute('role', 'document');
    this.wrapper.append(this.dialog);
    this.content = document.createElement('div');
    this.content.classList.add('modal-content');
    this.dialog.append(this.content);
    this.header = document.createElement('div');
    this.header.classList.add('modal-header');
    this.content.append(this.header);
    this.title = document.createElement('h4');
    this.title.classList.add('modal-title');
    this.header.append(this.title);
    this.closeBtn = document.createElement('button');
    this.closeBtn.setAttribute('type', 'button');
    this.closeBtn.setAttribute('aria-label', 'Close');
    this.closeBtn.classList.add('btn-close');
    this.closeBtn.dataset.bsDismiss = 'modal';
    this.header.append(this.closeBtn);
    this.body = document.createElement('div');
    this.body.classList.add('modal-body');
    this.content.append(this.body);
    this.modal = new bootstrap.Modal(this.wrapper);
  }
  show() {
    this.modal.show();
  }
}

/***/ }),

/***/ "./src/js/modals/ModalVisit.js":
/*!*************************************!*\
  !*** ./src/js/modals/ModalVisit.js ***!
  \*************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ModalVisit": () => (/* binding */ ModalVisit),
/* harmony export */   "default": () => (/* binding */ renderCards)
/* harmony export */ });
/* harmony import */ var _components_Select__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../components/Select */ "./src/js/components/Select.js");
/* harmony import */ var _utils_info__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../utils/info */ "./src/js/utils/info.js");
/* harmony import */ var _forms_FormCardiologist__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../forms/FormCardiologist */ "./src/js/forms/FormCardiologist.js");
/* harmony import */ var _forms_FormDentist__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../forms/FormDentist */ "./src/js/forms/FormDentist.js");
/* harmony import */ var _forms_FormTherapist__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../forms/FormTherapist */ "./src/js/forms/FormTherapist.js");
/* harmony import */ var _Modal__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./Modal */ "./src/js/modals/Modal.js");
/* harmony import */ var _utils_API__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../utils/API */ "./src/js/utils/API.js");
/* harmony import */ var _Visits_VisitCardiologist__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../Visits/VisitCardiologist */ "./src/js/Visits/VisitCardiologist.js");
/* harmony import */ var _Visits_VisitDentist__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../Visits/VisitDentist */ "./src/js/Visits/VisitDentist.js");
/* harmony import */ var _Visits_VisitTherapist__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../Visits/VisitTherapist */ "./src/js/Visits/VisitTherapist.js");
/* harmony import */ var _main__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../main */ "./src/js/main.js");











class ModalVisit extends _Modal__WEBPACK_IMPORTED_MODULE_5__.Modal {
  constructor(parent) {
    super(parent);
    this.saveBtn = document.createElement('button');
    this.saveBtn.classList.add('btn', 'btn-primary', 'create-card');
    this.saveBtn.innerText = 'Зберегти';
    this.saveEditedBtn = document.createElement('button');
    this.saveEditedBtn.classList.add('btn', 'btn-primary', 'edit-card');
    this.saveEditedBtn.innerText = 'Зберегти зміни';
  }
  show() {
    super.show();
    this.body.innerHTML = '';
    this.doctorSelect = new _components_Select__WEBPACK_IMPORTED_MODULE_0__["default"](_utils_info__WEBPACK_IMPORTED_MODULE_1__.info.doctors, 'Оберіть лікаря', true, 'form-select', 'doctor').render();
    this.body.append(this.doctorSelect);
    this.doctorSelect.addEventListener('change', event => {
      if (this.form) {
        this.form.innerHTML = '';
      }
      switch (event.target.value) {
        case _utils_info__WEBPACK_IMPORTED_MODULE_1__.info.cardiologist:
          this.form = new _forms_FormCardiologist__WEBPACK_IMPORTED_MODULE_2__.FormCardiologist().render();
          break;
        case _utils_info__WEBPACK_IMPORTED_MODULE_1__.info.dentist:
          this.form = new _forms_FormDentist__WEBPACK_IMPORTED_MODULE_3__.FormDentist().render();
          break;
        case _utils_info__WEBPACK_IMPORTED_MODULE_1__.info.therapist:
          this.form = new _forms_FormTherapist__WEBPACK_IMPORTED_MODULE_4__.FormTherapist().render();
          break;
      }
      this.form.addEventListener('submit', e => {
        if (!(e.submitter.tagName === "BUTTON" && e.submitter.classList.contains("create-card"))) return;
        e.preventDefault();
        const formData = new FormData(e.target);
        let dataObj = {};
        for (const p of formData) {
          let key = p[0],
            value = p[1];
          dataObj[key] = value;
        }
        (0,_utils_API__WEBPACK_IMPORTED_MODULE_6__.createCard)(dataObj).then(response => {
          if (response.ok) {
            console.log(response);
            renderCards();
            let closeBtn = document.querySelector('.btn-close');
            closeBtn.click();
          } else {
            throw new Error('Something went wrong');
          }
        });
      });
      this.title.innerText = 'Створити візит';
      this.form.append(this.saveBtn);
      this.body.append(this.form);
    });
  }
  showForEdit(id) {
    super.show();
    this.body.innerHTML = '';
    this.doctorSelect = new _components_Select__WEBPACK_IMPORTED_MODULE_0__["default"](_utils_info__WEBPACK_IMPORTED_MODULE_1__.info.doctors, 'Лікар:', true, 'form-select', 'doctor');
    (0,_utils_API__WEBPACK_IMPORTED_MODULE_6__.getCard)(id).then(response => response.json()).then(visit => {
      this.form.innerHTML = '';
      switch (visit.doctor) {
        case _utils_info__WEBPACK_IMPORTED_MODULE_1__.info.cardiologist:
          this.form = new _forms_FormCardiologist__WEBPACK_IMPORTED_MODULE_2__.FormCardiologist().renderForEdit(visit.id, visit.purpose, visit.description, visit.urgency, visit.patientName, visit.pressure, visit.bodyMassIndex, visit.pastDiseases, visit.age);
          break;
        case _utils_info__WEBPACK_IMPORTED_MODULE_1__.info.dentist:
          this.form = new _forms_FormDentist__WEBPACK_IMPORTED_MODULE_3__.FormDentist().renderForEdit(visit.id, visit.purpose, visit.description, visit.urgency, visit.patientName, visit.lastVisit);
          break;
        case _utils_info__WEBPACK_IMPORTED_MODULE_1__.info.therapist:
          this.form = new _forms_FormTherapist__WEBPACK_IMPORTED_MODULE_4__.FormTherapist().renderForEdit(visit.id, visit.purpose, visit.description, visit.urgency, visit.patientName, visit.age);
          break;
      }
      this.doctorSelect.value = visit.doctor;
      this.doctorSelect.select.disabled = true;
      this.body.append(this.doctorSelect.render());
      this.title.innerText = 'Редагувати візит';
      this.form.append(this.saveEditedBtn);
      this.body.append(this.form);
    }).then(() => {
      this.form.addEventListener('submit', e => {
        if (!(e.submitter.tagName === "BUTTON" && e.submitter.classList.contains("edit-card"))) return;
        e.preventDefault();
        const formData = new FormData(e.target);
        let dataObj = {};
        for (const p of formData) {
          let key = p[0],
            value = p[1];
          dataObj[key] = value;
        }
        (0,_utils_API__WEBPACK_IMPORTED_MODULE_6__.editCard)(dataObj, dataObj.id).then(response => {
          if (response.ok) {
            console.log(response);
            renderCards();
            let closeBtn = document.querySelector('.btn-close');
            closeBtn.click();
          } else {
            throw new Error('Something went wrong');
          }
        });
      });
    });
  }
}
function renderCards() {
  (0,_utils_API__WEBPACK_IMPORTED_MODULE_6__.getCards)().then(response => response.json()).then(cards => {
    console.log(cards);
    return cards;
  }).then(cardsArr => {
    const noItemsLabel = document.querySelector('.items-been');
    if (cardsArr.length !== 0) {
      noItemsLabel.style.display = 'none';
    } else {
      noItemsLabel.style.display = 'block';
    }
    const cardsWrapper = document.querySelector('.all-cards');
    cardsWrapper.innerHTML = '';
    cardsArr.forEach(item => {
      let card;
      switch (item.doctor) {
        case _utils_info__WEBPACK_IMPORTED_MODULE_1__.info.cardiologist:
          card = new _Visits_VisitCardiologist__WEBPACK_IMPORTED_MODULE_7__.VisitCardiologist(item.id, item.patientName, item.doctor, item.purpose, item.description, item.urgency, item.pressure, item.bodyMassIndex, item.pastDiseases, item.age).render();
          break;
        case _utils_info__WEBPACK_IMPORTED_MODULE_1__.info.dentist:
          card = new _Visits_VisitDentist__WEBPACK_IMPORTED_MODULE_8__.VisitDentist(item.id, item.patientName, item.doctor, item.purpose, item.description, item.urgency, item.lastVisit).render();
          break;
        case _utils_info__WEBPACK_IMPORTED_MODULE_1__.info.therapist:
        default:
          card = new _Visits_VisitTherapist__WEBPACK_IMPORTED_MODULE_9__.VisitTherapist(item.id, item.patientName, item.doctor, item.purpose, item.description, item.urgency, item.age).render();
          break;
      }
      cardsWrapper.append(card);
    });
  }).then(() => {
    const cardsWrapper = document.querySelector('.all-cards');
    cardsWrapper.addEventListener('click', e => {
      e.preventDefault();
      if (e.target.tagName === 'DIV' && e.target.classList.contains('delete-btn')) {
        e.preventDefault();
        let wrapper = e.target.closest(".card-wrapper");
        let id = wrapper.querySelector('.hidden-id').innerText;
        (0,_utils_API__WEBPACK_IMPORTED_MODULE_6__.deleteCard)(id).then(() => renderCards());
      }
    });
  }).then(() => {
    const cardsWrapper = document.querySelector('.all-cards');
    cardsWrapper.addEventListener('click', e => {
      e.preventDefault();
      if (e.target.tagName === 'DIV' && e.target.classList.contains('edit-btn')) {
        e.preventDefault();
        let wrapper = e.target.closest(".card-wrapper");
        let id = wrapper.querySelector('.hidden-id').innerText;
        _main__WEBPACK_IMPORTED_MODULE_10__.modal.showForEdit(id);
      }
    });
  });
}

/***/ }),

/***/ "./src/js/utils/API.js":
/*!*****************************!*\
  !*** ./src/js/utils/API.js ***!
  \*****************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "createCard": () => (/* binding */ createCard),
/* harmony export */   "deleteCard": () => (/* binding */ deleteCard),
/* harmony export */   "editCard": () => (/* binding */ editCard),
/* harmony export */   "getCard": () => (/* binding */ getCard),
/* harmony export */   "getCards": () => (/* binding */ getCards),
/* harmony export */   "login": () => (/* binding */ login)
/* harmony export */ });
//Authorization: `Bearer 4d472ffc-84d0-4d28-8409-afc98d26ba97`

const API_URL = 'https://ajax.test-danit.com/api/v2/cards';
const token = "4d472ffc-84d0-4d28-8409-afc98d26ba97";
//todo: let token = localStorage.getItem("token");

function login(email, password) {
  return fetch(`${API_URL}/login`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      email,
      password
    })
  });
}
function createCard(dataObj) {
  return fetch(`${API_URL}`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    body: JSON.stringify(dataObj)
  });
}
function getCards() {
  return fetch(`${API_URL}`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    }
  });
}
function getCard(id) {
  return fetch(`${API_URL}/${id}`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    }
  });
}
function deleteCard(id) {
  console.log('deleting ', id);
  return fetch(`${API_URL}/${id}`, {
    method: "DELETE",
    headers: {
      'Authorization': `Bearer ${token}`
    }
  });
}
function editCard(newCard, id) {
  return fetch(`${API_URL}/${id}`, {
    method: "PUT",
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    body: JSON.stringify(newCard)
  });
}

/***/ }),

/***/ "./src/js/utils/info.js":
/*!******************************!*\
  !*** ./src/js/utils/info.js ***!
  \******************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "info": () => (/* binding */ info)
/* harmony export */ });
const info = {
  cardiologist: "Кардіолог",
  dentist: "Стоматолог",
  therapist: "Терапевт",
  urgency: ["Звичайна", "Пріоритетна", "Невідкладна"],
  doctors: ["Кардіолог", "Стоматолог", "Терапевт"]
};

/***/ }),

/***/ "./src/js/utils/token.js":
/*!*******************************!*\
  !*** ./src/js/utils/token.js ***!
  \*******************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "dropToken": () => (/* binding */ dropToken),
/* harmony export */   "getToken": () => (/* binding */ getToken),
/* harmony export */   "setToken": () => (/* binding */ setToken)
/* harmony export */ });
function setToken(value) {
  localStorage.setItem('myToken', value);
}
function getToken() {
  return localStorage.getItem('myToken');
}
function dropToken() {
  localStorage.removeItem("myToken");
}

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/compat get default export */
/******/ 	(() => {
/******/ 		// getDefaultExport function for compatibility with non-harmony modules
/******/ 		__webpack_require__.n = (module) => {
/******/ 			var getter = module && module.__esModule ?
/******/ 				() => (module['default']) :
/******/ 				() => (module);
/******/ 			__webpack_require__.d(getter, { a: getter });
/******/ 			return getter;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module is referenced by other modules so it can't be inlined
/******/ 	var __webpack_exports__ = __webpack_require__("./src/js/main.js");
/******/ 	
/******/ })()
;
//# sourceMappingURL=main.js.map