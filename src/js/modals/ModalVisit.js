import Select from "../components/Select";
import {info} from "../utils/info";
import {FormCardiologist} from "../forms/FormCardiologist";
import {FormDentist} from "../forms/FormDentist";
import {FormTherapist} from "../forms/FormTherapist";
import {Modal} from "./Modal";
import {createCard, deleteCard, editCard, getCard, getCards} from "../utils/API";
import {VisitCardiologist} from "../Visits/VisitCardiologist";
import {VisitDentist} from "../Visits/VisitDentist";
import {VisitTherapist} from "../Visits/VisitTherapist";
import {modal} from "../main";

export class ModalVisit extends Modal {
    constructor(parent) {
        super(parent);

        this.saveBtn = document.createElement('button');
        this.saveBtn.classList.add('btn', 'btn-primary', 'create-card');
        this.saveBtn.innerText = 'Зберегти';


        this.saveEditedBtn = document.createElement('button');
        this.saveEditedBtn.classList.add('btn', 'btn-primary', 'edit-card');
        this.saveEditedBtn.innerText = 'Зберегти зміни';

    }

    show() {
        super.show();
        this.body.innerHTML = '';

        this.doctorSelect = new Select(info.doctors, 'Оберіть лікаря', true, 'form-select', 'doctor').render();

        this.body.append(this.doctorSelect);

        this.doctorSelect.addEventListener('change', (event) => {
            if (this.form) {
                this.form.innerHTML = '';
            }

            switch (event.target.value) {
                case info.cardiologist:
                    this.form = new FormCardiologist().render();
                    break;
                case info.dentist:
                    this.form = new FormDentist().render();
                    break;
                case info.therapist:
                    this.form = new FormTherapist().render();
                    break;
            }

            this.form.addEventListener('submit', e => {
                if(!(e.submitter.tagName === "BUTTON" && e.submitter.classList.contains("create-card")))
                    return;

                e.preventDefault();

                const formData = new FormData(e.target);
                    let dataObj={}
                for (const p of formData) {
                    let key = p[0], value = p[1];
                    dataObj[key]=value;
                }

                createCard(dataObj)
                    .then((response) => {
                        if(response.ok) {
                            console.log(response);

                            renderCards();

                            let closeBtn = document.querySelector('.btn-close');
                            closeBtn.click();
                        } else {
                            throw new Error('Something went wrong');
                        }
                    })
            })

            this.title.innerText = 'Створити візит';
            this.form.append(this.saveBtn);
            this.body.append(this.form);
        });
    }

    showForEdit(id) {
        super.show();

        this.body.innerHTML = '';
        this.doctorSelect = new Select(info.doctors, 'Лікар:', true, 'form-select', 'doctor');


        getCard(id)
            .then(response =>response.json())
            .then(visit => {
                this.form.innerHTML = '';

                switch (visit.doctor) {
                    case info.cardiologist:
                        this.form = new FormCardiologist()
                            .renderForEdit(visit.id, visit.purpose, visit.description, visit.urgency, visit.patientName,
                                visit.pressure, visit.bodyMassIndex, visit.pastDiseases, visit.age);
                        break;
                    case info.dentist:
                        this.form = new FormDentist()
                            .renderForEdit(visit.id, visit.purpose, visit.description, visit.urgency, visit.patientName,
                                visit.lastVisit);
                        break;
                    case info.therapist:
                        this.form = new FormTherapist()
                            .renderForEdit(visit.id, visit.purpose, visit.description, visit.urgency, visit.patientName,
                                visit.age);
                        break;
                }

                this.doctorSelect.value = visit.doctor;
                this.doctorSelect.select.disabled = true;
                this.body.append(this.doctorSelect.render());

                this.title.innerText = 'Редагувати візит';
                this.form.append(this.saveEditedBtn);
                this.body.append(this.form);
            })
            .then(()=>{
                this.form.addEventListener('submit', e => {
                    if(!(e.submitter.tagName === "BUTTON" && e.submitter.classList.contains("edit-card")))
                        return;

                    e.preventDefault();

                    const formData = new FormData(e.target);
                    let dataObj={}
                    for (const p of formData) {
                        let key = p[0], value = p[1];
                        dataObj[key]=value;
                    }

                    editCard(dataObj, dataObj.id)
                        .then((response) => {
                            if(response.ok) {
                                console.log(response);

                                renderCards();

                                let closeBtn = document.querySelector('.btn-close');
                                closeBtn.click();
                            } else {
                                throw new Error('Something went wrong');
                            }
                        })

                })
            })

    }
}

export default function renderCards() {
    getCards()
        .then(response => response.json())
        .then(cards => {console.log(cards); return cards})
        .then(cardsArr => {
            const noItemsLabel = document.querySelector('.items-been');

            if (cardsArr.length !== 0) {
                noItemsLabel.style.display = 'none';
            } else {
                noItemsLabel.style.display = 'block';
            }

            const cardsWrapper = document.querySelector('.all-cards');
            cardsWrapper.innerHTML = '';

            cardsArr.forEach(item=>{
                let card;
                switch (item.doctor){
                    case info.cardiologist:
                        card = new VisitCardiologist(
                            item.id, item.patientName, item.doctor, item.purpose,
                            item.description, item.urgency, item.pressure,
                            item.bodyMassIndex, item.pastDiseases, item.age).render();
                        break;
                    case info.dentist:
                        card = new VisitDentist(
                            item.id, item.patientName, item.doctor, item.purpose,
                            item.description, item.urgency, item.lastVisit).render();
                        break;
                    case info.therapist:
                    default:
                        card = new VisitTherapist(
                            item.id, item.patientName, item.doctor, item.purpose,
                            item.description, item.urgency, item.age).render();
                        break;
                }

                cardsWrapper.append(card);
            });
        })
        .then(() =>{
            const cardsWrapper = document.querySelector('.all-cards');

            cardsWrapper.addEventListener('click',  e => {
                e.preventDefault();

                if (e.target.tagName ==='DIV' && e.target.classList.contains('delete-btn')){
                    e.preventDefault();
                    let wrapper = e.target.closest(".card-wrapper");
                    let id = wrapper.querySelector('.hidden-id').innerText;
                    deleteCard(id).then(() => renderCards());
                }
            })
        })
        .then(()=> {
            const cardsWrapper = document.querySelector('.all-cards');
            cardsWrapper.addEventListener('click',  e => {
                e.preventDefault();

                if (e.target.tagName ==='DIV' && e.target.classList.contains('edit-btn')){
                    e.preventDefault();
                    let wrapper = e.target.closest(".card-wrapper");
                    let id = wrapper.querySelector('.hidden-id').innerText;

                    modal.showForEdit(id);
                }
            })
        })

}
