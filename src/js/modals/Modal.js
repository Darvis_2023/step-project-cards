export class Modal {
    constructor(parent) {
        this.wrapper = document.createElement('div');
        this.wrapper.setAttribute('id', 'myModal')
        this.wrapper.classList.add('modal', 'fade');
        this.wrapper.setAttribute('tabindex', '-1');
        this.wrapper.setAttribute('role','dialog');

        parent.append(this.wrapper);

        this.dialog = document.createElement('div');
        this.dialog.classList.add('modal-dialog', 'modal-dialog-centered', 'modal-lg');
        this.dialog.setAttribute('role','document');
        this.wrapper.append(this.dialog);

        this.content = document.createElement('div');
        this.content.classList.add('modal-content');
        this.dialog.append(this.content);

        this.header = document.createElement('div');
        this.header.classList.add('modal-header');
        this.content.append(this.header);

        this.title = document.createElement('h4');
        this.title.classList.add('modal-title');
        this.header.append(this.title);

        this.closeBtn = document.createElement('button');
        this.closeBtn.setAttribute('type', 'button');
        this.closeBtn.setAttribute('aria-label', 'Close');
        this.closeBtn.classList.add('btn-close');
        this.closeBtn.dataset.bsDismiss = 'modal';
        this.header.append(this.closeBtn);

        this.body = document.createElement('div');
        this.body.classList.add('modal-body');
        this.content.append(this.body);

        this.modal = new bootstrap.Modal(this.wrapper);
    }

    show(){
       this.modal.show();
    }
}



