import Input from "../components/Input";
import Textarea from "../components/Textarea";
import {Form} from "./Form";
import {info} from "../utils/info";

export class FormCardiologist extends Form {
    constructor() {
        super(info.cardiologist);

        this.pressure = new Input('Тиск:', 'text', 'Тиск', false,'form-control', 'pressure');
        this.bodyMassIndex = new Input('Індекс маси тіла:', 'text', 'Індекс маси тіла', false,'form-control', 'bodyMassIndex');
        this.pastDiseases =  new Textarea('Перенесені захворювання:','3', 'Перенесені захворювання', false,'form-control', 'pastDiseases');
        this.age = new Input('Вік:', 'text', 'Вік', false,'form-control', 'age');
    }
    render(){
        super.render();
        this.self.append(
            this.pressure.render(),
            this.bodyMassIndex.render(),
            this.pastDiseases.render(),
            this.age.render());

        return this.self;
    }

    renderForEdit(id, purpose, description, urgency, patientName, pressure, bodyMassIndex, pastDiseases, age){
        super.renderForEdit(id, purpose, description, urgency, patientName);

        this.pressure.value = pressure;
        this.bodyMassIndex.value = bodyMassIndex;
        this.pastDiseases.value = pastDiseases;
        this.age.value = age;

        this.self.append(
            this.pressure.render(),
            this.bodyMassIndex.render(),
            this.pastDiseases.render(),
            this.age.render());

        return this.self;
    }
}
