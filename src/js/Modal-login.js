const openPopUp = document.querySelector(".open_pop_up");
const closePopUp = document.querySelector(".pop_up_close");
const popUp = document.querySelector(".pop_up_authorization");

openPopUp.addEventListener("click", (e) => {
    e.preventDefault();
    popUp.style.display = "block";
});

closePopUp.addEventListener("click", () => {
    popUp.style.display = "none";
});

function showPass() {
	const show = document.querySelector(".pass-btn");
	const input = document.querySelector(".password-input");

	show.addEventListener("click", () => {
		show.classList.toggle('active');

		if (input.getAttribute('type') === 'password') {
			input.setAttribute('type', 'text');
		} else {
			input.setAttribute('type', 'password');
		}
	});
}

showPass();