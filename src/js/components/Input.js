export default class Input {
    input;
    constructor(labelText, type, placeholder, isRequired, className, name, additionalAttributes = []) {
        this.type = type;
        this.placeholder = placeholder;
        this.isRequired = isRequired;
        this.className = className;
        this.name = name;
        this.additionalAttributes = additionalAttributes;
        this.self = document.createElement('div');
        this.labelText = labelText;
        this.label = document.createElement('label');
        this.input = document.createElement('input');
    }

    set value(text){
        this.input.value = text;
    }

    render(){
        if(this.type === "hidden")
            this.self.classList.add("d-none");

        this.label.classList.add('form-label');
        this.label.innerText = this.labelText;
        this.self.append(this.label);

        this.input.setAttribute('type', this.type);
        this.input.setAttribute('placeholder', this.placeholder);
        this.input.setAttribute('name', this.name);
        this.input.required = this.isRequired;
        this.input.classList.add(this.className);
        this.additionalAttributes.forEach(attr =>{
            this.input.setAttribute(attr.name, attr.value);
        });
        this.self.append(this.input);

        return this.self;
    }
}