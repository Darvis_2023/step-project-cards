export default class Textarea {
    constructor(labelText, rows, placeholder, isRequired, className, name, additionalAttributes = []) {
        this.rows = rows;
        this.placeholder = placeholder;
        this.isRequired = isRequired;
        this.className = className;
        this.name = name;
        this.additionalAttributes = additionalAttributes;
        this.self = document.createElement('div');
        this.labelText = labelText;
        this.label = document.createElement('label');
        this.textarea = document.createElement('textarea');
    }

    set value(text){
        this.textarea.value = text;
    }

    render(){
        this.label.classList.add('form-label');
        this.label.innerText = this.labelText;
        this.self.append(this.label);

        this.textarea.setAttribute('rows', this.rows);
        this.textarea.setAttribute('placeholder', this.placeholder);
        this.textarea.setAttribute('name', this.name);
        this.textarea.required = this.isRequired;
        this.textarea.classList.add(this.className);
        this.additionalAttributes.forEach(attr =>{
            this.textarea.setAttribute(attr.name, attr.value);
        });
        this.self.append(this.textarea);

        return this.self;
    }
}