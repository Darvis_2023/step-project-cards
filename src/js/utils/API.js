//Authorization: `Bearer 4d472ffc-84d0-4d28-8409-afc98d26ba97`


const API_URL = 'https://ajax.test-danit.com/api/v2/cards';
const token = "4d472ffc-84d0-4d28-8409-afc98d26ba97";
    //todo: let token = localStorage.getItem("token");

export function login (email, password){
    return fetch(`${API_URL}/login`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ email, password })
    })
}
export function createCard(dataObj) {
    return fetch(`${API_URL}`,{
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        },
        body: JSON.stringify(dataObj)
    })
}

export function getCards(){
    return fetch(`${API_URL}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        },
    })

}

export function getCard(id){
    return fetch(`${API_URL}/${id}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        },
    })

}
export function deleteCard(id) {
    console.log('deleting ', id);
    return fetch(`${API_URL}/${id}`,{
        method: "DELETE",
        headers: {
            'Authorization': `Bearer ${token}`
        }
    });
}

export function editCard(newCard, id) {
    return fetch(`${API_URL}/${id}`,{
        method: "PUT",
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        },
        body: JSON.stringify(newCard)
    })
}


