export function setToken (value){
    localStorage.setItem('myToken', value);
}

export function getToken(){
    return localStorage.getItem('myToken');
}

export function dropToken(){
    localStorage.removeItem("myToken");
}