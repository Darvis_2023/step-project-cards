import { Visit } from "./Visit";

export class VisitTherapist extends Visit {
  constructor(id, patientName, doctor, purpose, description, urgency, age) {
    super(id, patientName, doctor, purpose, description, urgency);

    this.data = {
      ...this.data,
      age,
    };
  }
  render() {
    super.render();

    this.age = document.createElement("p");
    this.age.innerText = `Вік: ${this.data.age}`;

    this.showMoreInfo.append(this.age);

    return this.wrapper;
  }
}
